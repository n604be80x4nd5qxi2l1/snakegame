// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ASpawner::ASpawner()
{
	PrimaryActorTick.bCanEverTick = true;
	bGetBorders = true;
	TickInterval = 0.5f;
	ElementSize = 50;
	SpawnChance = 0.5f;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(TickInterval);
}

bool ASpawner::LocationHaveObject(const FVector& Point)
{
	FHitResult HitResult;
	FCollisionQueryParams TraceParams;
	FVector Start{Point.X-ElementSize/2+10, Point.Y-ElementSize/2+10, Point.Z};
	FVector End{Point.X+ElementSize/2-10, Point.Y+ElementSize/2-10, Point.Z};
	return GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_Visibility, TraceParams);
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GetBorders();
	bool bSpawn = UKismetMathLibrary::RandomBoolWithWeight(SpawnChance);
	if (bSpawn)
	{
		SpawnObject(ActorToSpawn);
	};
}

void ASpawner::SpawnObject(UClass* ClassToSpawn)
{
	auto x = FMath::RandRange(-BorderX, BorderX);
	auto y = FMath::RandRange(-BorderY, BorderY);
	auto ActorLocation = FVector(x*ElementSize, y*ElementSize, 0);
	uint8 Tries(0);
	while (LocationHaveObject(ActorLocation))
	{
		if (++Tries > 9) return;
		x = FMath::RandRange(-BorderX, BorderX);
		y = FMath::RandRange(-BorderY, BorderY);
		ActorLocation = FVector(x*ElementSize, y*ElementSize, 0);
	}
	auto ActorTransform = FTransform(ActorLocation);
	GetWorld()->SpawnActor<AActor>(ClassToSpawn, ActorTransform);
		
}

void ASpawner::SetElementSize(const int32& NewElementSize)
{
	ElementSize = NewElementSize;
}

void ASpawner::GetBorders()
{
	if (bGetBorders)
	{
		FVector2D ViewportSize;
		GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);
		BorderX = (static_cast<int32>(ViewportSize.Y) / ElementSize);
		BorderY = (static_cast<int32>(ViewportSize.X) / ElementSize);
		bGetBorders = false;
	}
}

