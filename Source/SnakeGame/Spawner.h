// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class SNAKEGAME_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(EditDefaultsOnly, meta=(ClampMin = "0.0", ClampMax = "1.0"))
	float TickInterval;

	UPROPERTY(EditDefaultsOnly, meta=(ClampMin = "0.0", ClampMax = "1.0"))
	float SpawnChance;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor> ActorToSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	int32 BorderX, BorderY;
	int32 ElementSize;
	bool bGetBorders;
	
	void GetBorders();
	bool LocationHaveObject(const FVector& Point);
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SpawnObject(UClass* ClassToSpawn);

	UFUNCTION()
	void SetElementSize(const int32& NewElementSize);
	
};
