// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASpawner;
class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	float ElementSize;

	UPROPERTY(BlueprintReadWrite)
	float MovementSpeed;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	ASpawner* Spawner;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpawner> SpawnerClass;

	UPROPERTY()
	EMovementDirection LastMovementDirection;

	UPROPERTY()
	bool bCanChangeDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	int32 BorderX, BorderY;
	bool bGetBorders;
	bool bDead;

	void GetBorders();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION()
	void Move();
	UFUNCTION(BlueprintNativeEvent)
	void SnakeDead();
	void SnakeDead_Implementation();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

};
