// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Spawner.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 50.01f;
	MovementSpeed = 0.3f;
	LastMovementDirection = EMovementDirection::UP;
	bCanChangeDirection = false;
	bGetBorders = true;
	bDead = false;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	Spawner = GetWorld()->SpawnActor<ASpawner>(SpawnerClass, FTransform());
	Spawner->SetElementSize(static_cast<int32>(ElementSize));
	AddSnakeElement(2);
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::GetBorders()
{
	if (bGetBorders)
	{
		FVector2D ViewportSize;
		GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);
		BorderX = static_cast<int32>(ViewportSize.Y);
		BorderY = static_cast<int32>(ViewportSize.X);
		bGetBorders = false;
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GetBorders();

	if (GetActorTickInterval() != MovementSpeed)
		SetActorTickInterval(MovementSpeed);
	if (!bDead)
		Move();
	
	bCanChangeDirection = true;

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i(0); i<ElementsNum; ++i)
	{
		auto NewLocation(FVector(SnakeElements.Num()*ElementSize, 0, 0));
		if (SnakeElements.Num() > 0)
		{
			NewLocation = SnakeElements[SnakeElements.Num()-1]->GetActorLocation();
		}
		auto NewTransform = FTransform(NewLocation);
		auto NewSnakeElement(GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform));
		NewSnakeElement->SnakeOwner = this;
		auto ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);

	switch(LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (auto i = SnakeElements.Num()-1; i>0; --i)
	{
		auto CurrentElement(SnakeElements[i]);
		auto PrevElement(SnakeElements[i-1]);
		FVector PrevLocation(PrevElement->GetActorLocation());
		CurrentElement->SetActorLocation(PrevLocation);
	}
	
	
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	auto SnakeHeadLocation = SnakeElements[0]->GetActorLocation();
	if (SnakeHeadLocation.X < (-BorderX))
		SnakeElements[0]->SetActorLocation(FVector(-SnakeHeadLocation.X-ElementSize, SnakeHeadLocation.Y, 0));
	if (SnakeHeadLocation.Y < (-BorderY))
		SnakeElements[0]->SetActorLocation(FVector(SnakeHeadLocation.X, -SnakeHeadLocation.Y-ElementSize, 0));
	if (SnakeHeadLocation.X > (BorderX))
		SnakeElements[0]->SetActorLocation(FVector(-SnakeHeadLocation.X+ElementSize, SnakeHeadLocation.Y, 0));
	if (SnakeHeadLocation.Y > (BorderY))
		SnakeElements[0]->SetActorLocation(FVector(SnakeHeadLocation.X, -SnakeHeadLocation.Y+ElementSize, 0));
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeDead_Implementation()
{
	bDead = true;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		bool bIsFirst = SnakeElements.Find(OverlappedElement) == 0;

		IInteractable* InteractableInterface(Cast<IInteractable>(Other));
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

