// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "SnakeBase.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorLocation(FVector(0, 0, 3000));
	SetActorRotation(FRotator(-90,0,0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("Exit", IE_Pressed, this, &APlayerPawnBase::HandleExit);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float Value)
{
	if (IsValid(SnakeActor) && SnakeActor->bCanChangeDirection)
	{
		if (Value > 0 && SnakeActor->LastMovementDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::UP;
			SnakeActor->bCanChangeDirection = false;
		}
		else if (Value < 0 && SnakeActor->LastMovementDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::DOWN;
			SnakeActor->bCanChangeDirection = false;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeActor) && SnakeActor->bCanChangeDirection)
	{
		if (Value > 0 && SnakeActor->LastMovementDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::RIGHT;
			SnakeActor->bCanChangeDirection = false;
		}
		else if (Value < 0 && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::LEFT;
			SnakeActor->bCanChangeDirection = false;
		}
	}
}

void APlayerPawnBase::HandleExit()
{
	FGenericPlatformMisc::RequestExit(true);
}

